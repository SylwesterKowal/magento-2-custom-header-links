<?php


namespace Kowal\CustomHeaderLinks\Block;

use Magento\Framework\Math\Random;
use Magento\Framework\View\Helper\SecureHtmlRenderer;

class CustomHeaderLinks extends \Magento\Framework\View\Element\Html\Link
{
    protected $_scopeConfig;


    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param SecureHtmlRenderer|null $secureRenderer
     * @param Random|null $random
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null,
        ?Random $random = null
    )
    {

        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data,$secureRenderer, $random);
    }


    /**
     * Render block HTML.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (false != $this->getTemplate()) {
            return parent::_toHtml();
        }
//        $label = $this->escapeHtml($this->getLabel());
//        return '<li><a ' . $this->getLinkAttributes() . ' >' . $label . '</a></li>';

        $columns = $this->getColumns();

        $content = "";
        for ($i = 1; $i <= $columns; $i++) {
            $cmsBlock = $this->getContentBlock($i);
            $cmsBlock = "<span class=\"kowal-custom-header-link-{$i}\">{$cmsBlock}</span>";
            $content .= $cmsBlock;
        }
        return $content;
    }


    /**
     * @return int
     */
    public function getColumns()
    {
        return (int)$this->getConfig('customheaderlinks/general/ilosc_kolumn');
    }

    /**
     * @param $path
     * @return string
     */
    public function getConfig($path)
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        return (string)$this->_scopeConfig->getValue($path, $storeScope);
    }

    /**
     * @param $i
     * @return string
     */
    public function getContentBlock($i)
    {
        return (string)$this->getLayout()
            ->createBlock('Magento\Cms\Block\Block')
            ->setBlockId($this->getConfig('customheaderlinks/customheaderlinks/kolumna_' . $i))
            ->toHtml();

    }

    /**
     * @param $i
     * @return string
     */
    public function getContentHeader($i)
    {
        $myCmsBlock = $this->getLayout()
            ->createBlock('Kowal\CustomHeaderLinks\Block\Cms\Block')
            ->setBlockId($this->getConfig('customheaderlinks/customheaderlinks/kolumna_' . $i));
        return (string)$myCmsBlock->getTitle();
    }

}
